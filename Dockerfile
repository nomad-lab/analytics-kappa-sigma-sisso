# Simple Docker image with Anaconda Python 3, boost, build-essential
ARG BUILDER_BASE_IMAGE=jupyter/scipy-notebook:python-3.9
FROM $BUILDER_BASE_IMAGE

USER root

MAINTAINER Thomas Purcell <purcell@fhi-berlin.mpg.de>

ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Europe/Berlin"

RUN apt-get update &&\
    apt-get install -y build-essential g++ cmake git vim xterm &&\
    apt-get install -y liblapack-dev libblas-dev &&\
    apt-get install -y openssh-client openssh-server rsync &&\
    apt-get install -y openmpi-bin openmpi-common libopenmpi-dev &&\
    apt-get install -y doxygen &&\
    apt-get install -y pkgconf &&\
    apt-get install -y zlib1g-dev &&\
    apt-get install -y libboost-system-dev libboost-filesystem-dev libboost-mpi-dev libboost-serialization-dev &&\
    apt-get install -y r-base &&\
    apt-get install -y texlive-full &&\
    apt-get install -y libfmt-dev &&\
    apt-get install -y libnlopt-dev libnlopt-cxx-dev &&\
    apt-get install -y coinor-libclp-dev &&\
    apt-get clean

# Setup the user
RUN useradd -d /home/runner -ms /bin/bash runner

RUN mamba install --quiet --yes \
    'numpy' \
    'pandas' \
    'scipy' \
    'seaborn' \
    'scikit-learn' \
    'toml' \
    'pytest' \
    'plotly' \
    'shap' \
    'SALib' \
    'rpy2' \
    'lime' \
 && mamba clean --all -f -y \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

RUN Rscript -e "install.packages(c('shapr'), repos='https://cran.rstudio.com')"

WORKDIR /opt/sissopp
COPY third_party/sissopp/ .
RUN mkdir build &&\
    cd build/ &&\
    cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_FLAGS="-O3" -DBUILD_PYTHON=ON -DBUILD_PARAMS=ON -DBUILD_EXE=OFF -DBUILD_TESTS=OFF ../ &&\
    make &&\
    make install &&\
    make test


USER ${NB_UID}
WORKDIR /home/${NB_USER}

RUN mkdir notebook
COPY docker/notebook.tar.gz  notebook/
RUN cd notebook/ &&\
    tar xzf notebook.tar.gz 

#ENV DOCKER_STACKS_JUPYTER_CMD="nbclassic"
